import React, {Component} from 'react';
import './App.css';
import Header from './components/Header';
import {Provider} from 'react-redux';

import store from './store/';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
/* Components */
import NoMatch from './components/NoMatch';
import AddNewFlight from './components/AddNewFlight';
import Flights from './components/Flights';
import Login from './components/Login';

class App extends Component {
    render() {
        return (
            <div className="App">
            <h1>Flights App</h1>
            <Provider store={store}>
                <Router>
                    <div>
                        <Header/>
                        <Switch>
                            <Route path="/" component={Login} exact/>
                            <Route path="/Flights" component={Flights}/>
                            <Route path="/AddNewFlight" component={AddNewFlight}/>
                            <Route component={NoMatch}/>
                        </Switch>
                    </div>
                </Router>
                </Provider>
            </div>
        );
    }
}

export default App;
