import {combineReducers ,createStore} from 'redux';


const loginState = {
    user: '',
    password: '',
    submit : false,
    error: false
}

const flightsState = {
    flights: [
       
    ],
}

const loginReducer = (state = loginState, action) => {
    switch (action.type) {
        case  'INIT':
            return Object.assign({}, state, {user:'',password:'',submit: false,error:false});
        case 'USER_CHANGE':
            return Object.assign({}, state, {user: action.data});
        case 'PASSWORD_CHANGE':
            return Object.assign({}, state, {password: action.data});
        case 'SUBMIT':
            if (state.user == 'user' && state.password == 'password')
                return Object.assign({}, state, {submit: true,error:false});
            return Object.assign({}, state, {submit: true,error:true});
        default:
            return state
    }
}
const flightsReducer = (state = flightsState, action) => {
    switch (action.type) {
        case 'ADD':
            return Object.assign({}, state, {flights:[...state.flights,action.data]});
        default:
            return state
    }
}


const reducers = combineReducers({
    flightsReducer,
    loginReducer
})
const store = createStore(reducers);

export default store;
