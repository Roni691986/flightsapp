import React, {Component} from 'react';
import {connect} from 'react-redux';
class AddNewFlight extends Component {
    render() {
        return (
            <div>
                <h1>Add new flight page!</h1>
                <form    onSubmit={(e) => {  this.props.SUBMIT(e) }}>
                    <div className="form-row align-items-center">
                        <div className="col-auto">
                            <label className="sr-only" htmlFor="inlineFormInput">From</label>
                            <input type="text" className="form-control mb-2" id="from" placeholder="From"
                                ref="from"
                            /></div>
                        <div className="col-auto">
                            <label className="sr-only" htmlFor="inlineFormInput">To</label>
                            <input type="text" className="form-control mb-2" id="to" placeholder="To"
                             ref="to"/></div>
                        <div className="col-auto">
                            <label className="sr-only" htmlFor="inlineFormInput">Departure</label>
                            <input type="date" className="form-control mb-2" id="departure" placeholder="Departure"
                             ref="departure"/></div>
                            <div className="col-auto">
                            <label className="sr-only" htmlFor="inlineFormInput">Landing Time</label>
                            <input type="time" className="form-control mb-2" id="landing_time" placeholder="Landing Time"
                            ref="landing_time" /></div>
                            <div className="col-auto">
                            <label className="sr-only" htmlFor="inlineFormInput">Price</label>
                            <input type="number" className="form-control mb-2" id="price" placeholder="price"
                              ref="price"/></div>
                        <div className="col-auto">
                            <button type="submit" className="btn btn-primary mb-2">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {flights: state.flightsReducer.flights};
}
function mapDispatchToProps(dispatch) {
    return {
        SUBMIT: (event) => {
            event.preventDefault();
            console.log(event);
            const action = {
                type: 'ADD',
                data: {
                    from:event.target.from.value,
                    to:event.target.to.value,
                    departure:event.target.departure.value,
                    landing_time:event.target.landing_time.value,
                    price:event.target.price.value,
                }
            };
            dispatch(action);
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddNewFlight);