import React, {Component} from 'react';
import {Navbar, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import {BrowserRouter as Router, Route, Switch, NavLink} from "react-router-dom";

class Header extends Component {
    render() {
        let nav = '';
        if (this.props.submit === true && this.props.error === false)
            nav = ( <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <NavLink to="/" exact   onClick={(e) => {  this.props.INIT(e) }}>
                            Logout
                        </NavLink>
                    </Navbar.Brand>
                </Navbar.Header>
                <div>
                    <NavLink to="/Flights">
                        <button type="button" className="btn btn-warning m-2">Flights</button>

                    </NavLink>
                    <NavLink to="/AddNewFlight">
                    <button type="button" className="btn btn-warning m-2">AddNewFlight</button>
                    </NavLink>
                </div>

            </Navbar>)
        return (
            <div>
               {nav}
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {user: state.loginReducer.user, 
        password: state.loginReducer.password,
        submit:state.loginReducer.submit,
        error:state.loginReducer.error,

    };
}
function mapDispatchToProps(dispatch) {
    return {
        INIT: () => {
            const action = {
                type: 'INIT',
            };
            dispatch(action);
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);