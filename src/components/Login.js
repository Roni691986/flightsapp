import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router';

class Login extends Component {
    render() {
        let error = '';
        if (this.props.submit === true && this.props.error === false) {
            return <Redirect to="/Flights"/>
        }
        if (this.props.submit === true && this.props.error === true) {
          this.error = 'Please provide your user and password correctly!';
        }
      
        return (
            <div>
                <form className="form-inline center"   
                    onSubmit={(e) => {  this.props.SUBMIT(e) }}>
                    <div className="form-group mb-2">
                        <label htmlFor="username" className="sr-only">User</label>
                        <input type="text" className="form-control" id="username" placeholder="User"
                          onChange={(e) => this.props.userChange(e.target.value)}/>
                    </div>
                    <div className="form-group mx-sm-3 mb-2">
                        <label htmlFor="inputPassword2" className="sr-only">Password</label>
                        <input
                            type="password"
                            className="form-control"
                            id="inputPassword2"
                            placeholder="Password"
                            onChange={(e) => this.props.passwordChange(e.target.value)}/>
                    </div>
                    <button type="submit" className="btn btn-primary mb-2">Confirm</button>
                   <div className="text-red"> {this.error}</div>
                </form>
            </div>
        )
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {user: state.loginReducer.user, 
        password: state.loginReducer.password,
        submit : state.loginReducer.submit,
        error :state.loginReducer.error 
    };
}
function mapDispatchToProps(dispatch) {
    return {
        initState: () => {
            const action = {
                type: 'INIT',
            };
            dispatch(action);
        },
        userChange: (user) => {
            const action = {
                type: 'USER_CHANGE',
                data: user
            };
            dispatch(action);
        },
        passwordChange: (password) => {
            const action = {
                type: 'PASSWORD_CHANGE',
                data: password
            };
            dispatch(action);
        },
        SUBMIT: (event) => {
            event.preventDefault();
            const action = {
                type: 'SUBMIT'
            };
            dispatch(action);
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);