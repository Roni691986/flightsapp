import React, {Component} from 'react';
import {connect} from 'react-redux';

class Flights extends Component {
    render() {
        let error = '';
        if (this.props.flights.length == 0)
            error = "No Flights Listed!";
        else
            {
                error = <table className="table table-dark">
                <thead>
                    <tr>
                        <th scope="col">From</th>
                        <th scope="col">To</th>
                        <th scope="col">Departure</th>
                        <th scope="col">Landing Time</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {this
                        .props
                        .flights
                        .map((flight, index)  => <tr key={index}>
                            <td>{flight.from}</td>
                            <td>{flight.to}</td>
                            <td>{flight.departure}</td>
                            <td>{flight.landing_time}</td>
                            <td>{flight.price}</td>
                        </tr>)}
                </tbody>
            </table>
            }
        return (
            <div>
                {error}
            </div>
        );
    }
}
function mapStateToProps(state) {
    console.log(state);
    return {flights: state.flightsReducer.flights};
}
function mapDispatchToProps(dispatch) {
    return {
       
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Flights);